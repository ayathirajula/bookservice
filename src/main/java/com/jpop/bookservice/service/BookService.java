package com.jpop.bookservice.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.jpop.bookservice.entity.Book;
import com.jpop.bookservice.exception.NoRecordFoundException;
import com.jpop.bookservice.repository.BookRepository;

@Component
public class BookService {

	@Autowired
	BookRepository bookRepository;

	public Book findBook(Long id) {
		return bookRepository.findById(id).get();
	}

	public List<Book> getAllBooks() {
		return bookRepository.findAll();
	}

	public Book saveBook(Book book) {
		return bookRepository.save(book);
	}

	public Book updateBook(Book book) {
		Book bookEntity = bookRepository.findById(book.getId()).orElseThrow(() -> new NoRecordFoundException(book.getId()));		
		bookEntity.setName(book.getName());
		bookEntity.setAuthor(book.getAuthor());
		bookEntity.setPublisher(book.getPublisher());
		return bookRepository.save(bookEntity);		
	}

	public void deleteBook(Long id) {
		bookRepository.deleteById(id);
	}

}
