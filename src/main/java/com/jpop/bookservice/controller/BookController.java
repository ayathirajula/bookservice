package com.jpop.bookservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.jpop.bookservice.entity.Book;
import com.jpop.bookservice.exception.NoRecordFoundException;
import com.jpop.bookservice.service.BookService;

@RestController
public class BookController {

	@Autowired
	BookService bookService;

	// Find All
	@GetMapping("/books")
	@ResponseStatus(HttpStatus.OK)
	public List<Book> findAll() {
		return bookService.getAllBooks();
	}

	// Find
	@GetMapping("/books/{id}")
	@ResponseStatus(HttpStatus.OK)
	public Book findBook(@PathVariable Long id) {
		return bookService.findBook(id);
	}

	// Save
	@PostMapping("/books")
	@ResponseStatus(HttpStatus.CREATED)
	public Book createNewBook(@RequestBody Book book) {
		return bookService.saveBook(book);
	}

	// update
	@PutMapping("/books")
	@ResponseStatus(HttpStatus.OK)
	public Book updteBook(@RequestBody Book book) {
		return bookService.updateBook(book);
	}

	// delete
	@DeleteMapping("/books/{id}")
	@ResponseStatus(HttpStatus.OK)
	public String deleteBook(@PathVariable Long id) {
		bookService.deleteBook(id);
		return "Book with id " + id + " is deleted.";
	}

}
