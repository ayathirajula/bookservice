package com.jpop.bookservice.exception;

public class NoRecordFoundException extends RuntimeException {  
    
	private static final long serialVersionUID = 1L;

	public NoRecordFoundException(Object resourId) {
        super(resourId != null ? "No record found with id :: " +resourId.toString() : null);
    }
}